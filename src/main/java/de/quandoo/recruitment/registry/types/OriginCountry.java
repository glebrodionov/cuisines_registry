package de.quandoo.recruitment.registry.types;

public enum OriginCountry {
    Germany,
    France,
    Italy,
    Spain
}
