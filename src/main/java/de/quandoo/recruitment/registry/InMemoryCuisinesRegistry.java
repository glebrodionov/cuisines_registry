package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.stream.Stream;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private Map<Cuisine, Set<Customer>> cuisineCustomers = new HashMap<>();
    private Map<Customer, Set<Cuisine>> customerCuisines = new HashMap<>();

    private Map<Cuisine, Integer> topCuisines = new LinkedHashMap<>();

    @Override
    public void register(final Customer customer, final Cuisine cuisine) {
        subscribeCustomerToCuisine(customer, cuisine);
        subscribeCuisineToCustomer(cuisine, customer);

        addToTopCuisines(cuisine);
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        Set<Customer> customers = cuisineCustomers.get(cuisine);
        return customers != null ? new ArrayList<>(customers) : null;
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        Set<Cuisine> cuisines = customerCuisines.get(customer);
        return cuisines != null ? new ArrayList<>(cuisines) : null;
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        sortTopCuisines();

        List<Cuisine> resultCuisines = new ArrayList<>();

        List<Cuisine> sortedTopCuisinesList = new ArrayList<>(topCuisines.keySet());

        for (int i = 0; i < n; i++) {
            resultCuisines.add(sortedTopCuisinesList.get(i));
        }

        return resultCuisines;
    }

    private void addToTopCuisines(final Cuisine cuisine) {
        if (topCuisines.containsKey(cuisine)) {
            topCuisines.put(cuisine, topCuisines.get(cuisine) + 1);
        } else {
            topCuisines.put(cuisine, 1);
        }
    }

    private void sortTopCuisines() {
        Stream<Map.Entry<Cuisine, Integer>> topCuisinesStream = topCuisines.entrySet().stream();
        topCuisinesStream.sorted(Map.Entry.comparingByValue()).forEachOrdered(c ->
                topCuisines.put(c.getKey(), c.getValue()));
    }

    private void subscribeCustomerToCuisine(final Customer customer, final Cuisine cuisine) {
        if (cuisineCustomers.containsKey(cuisine)) {
            cuisineCustomers.get(cuisine).add(customer);
        } else {
            Set<Customer> customers = new HashSet<>();
            customers.add(customer);
            cuisineCustomers.put(cuisine, customers);
        }
    }

    private void subscribeCuisineToCustomer(final Cuisine cuisine, final Customer customer) {
        if (customerCuisines.containsKey(customer)) {
            customerCuisines.get(customer).add(cuisine);
        } else {
            Set<Cuisine> cuisines = new HashSet<>();
            cuisines.add(cuisine);
            customerCuisines.put(customer, cuisines);
        }
    }

    // for testing purposes
    Set<Cuisine> getCuisinesByCustomer(final Customer customer) {
        return customerCuisines.get(customer);
    }

    // for testing purposes
    Set<Customer> getCustomersByCuisines(final Cuisine cuisine) {
        return cuisineCustomers.get(cuisine);
    }
}
