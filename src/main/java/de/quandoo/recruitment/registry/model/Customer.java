package de.quandoo.recruitment.registry.model;

public class Customer {
    private long id;

    public Customer(final long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }
}
