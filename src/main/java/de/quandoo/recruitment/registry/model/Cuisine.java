package de.quandoo.recruitment.registry.model;

import de.quandoo.recruitment.registry.types.OriginCountry;

public class Cuisine {

    private OriginCountry originCountry;

    public Cuisine(final OriginCountry originCountry) {
        this.originCountry = originCountry;
    }

    public OriginCountry getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(final OriginCountry originCountry) {
        this.originCountry = originCountry;
    }
}
