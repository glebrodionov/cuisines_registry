package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import de.quandoo.recruitment.registry.types.OriginCountry;
import org.junit.Test;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class InMemoryCuisinesRegistryTest {
    private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();


    @Test
    public void register() {
        Cuisine cuisine = new Cuisine(OriginCountry.Germany);
        Customer customer = new Customer(0);

        cuisinesRegistry.register(customer, cuisine);

        Set<Cuisine> cuisines = cuisinesRegistry.getCuisinesByCustomer(customer);
        assertEquals(1, cuisines.size());
        assertTrue(cuisines.contains(cuisine));

        Set<Customer> customers = cuisinesRegistry.getCustomersByCuisines(cuisine);
        assertEquals(1, customers.size());
        assertTrue(customers.contains(customer));

    }

    @Test
    public void cuisineCustomers() {
        List<Customer> customers = cuisinesRegistry.cuisineCustomers(null);
        assertNull(customers);

        Cuisine cuisine = new Cuisine(OriginCountry.Germany);
        Customer customer = new Customer(0);

        cuisinesRegistry.register(customer, cuisine);
        cuisinesRegistry.register(customer, cuisine);

        customers = cuisinesRegistry.cuisineCustomers(cuisine);
        assertEquals(1, customers.size());
        assertTrue(customers.contains(customer));

        Cuisine cuisine2 = new Cuisine(OriginCountry.France);
        cuisinesRegistry.register(customer, cuisine2);

        customers = cuisinesRegistry.cuisineCustomers(cuisine2);
        assertEquals(1, customers.size());
        assertTrue(customers.contains(customer));

        Customer customer2 = new Customer(1);
        cuisinesRegistry.register(customer2, cuisine);

        customers = cuisinesRegistry.cuisineCustomers(cuisine);
        assertEquals(2, customers.size());
        assertTrue(customers.contains(customer));
        assertTrue(customers.contains(customer2));
    }

    @Test
    public void customerCuisines() {
        List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(null);
        assertNull(cuisines);

        Cuisine cuisine = new Cuisine(OriginCountry.Germany);
        Customer customer = new Customer(0);

        cuisinesRegistry.register(customer, cuisine);
        cuisinesRegistry.register(customer, cuisine);

        cuisines = cuisinesRegistry.customerCuisines(customer);
        assertEquals(1, cuisines.size());
        assertTrue(cuisines.contains(cuisine));

        Cuisine cuisine2 = new Cuisine(OriginCountry.France);
        cuisinesRegistry.register(customer, cuisine2);

        cuisines = cuisinesRegistry.customerCuisines(customer);
        assertEquals(2, cuisines.size());
        assertTrue(cuisines.contains(cuisine));
        assertTrue(cuisines.contains(cuisine2));

        Customer customer2 = new Customer(1);
        cuisinesRegistry.register(customer2, cuisine);

        cuisines = cuisinesRegistry.customerCuisines(customer2);
        assertEquals(1, cuisines.size());
        assertTrue(cuisines.contains(cuisine));
    }

    @Test
    public void topCuisines() {
        Cuisine germanCuisine = new Cuisine(OriginCountry.Germany);
        Cuisine frenchCuisine = new Cuisine(OriginCountry.France);
        Cuisine italianCuisine = new Cuisine(OriginCountry.Italy);
        Cuisine spanishCuisine = new Cuisine(OriginCountry.Spain);

        Customer customer1 = new Customer(0);
        Customer customer2 = new Customer(1);
        Customer customer3 = new Customer(2);

        cuisinesRegistry.register(customer1, germanCuisine);
        cuisinesRegistry.register(customer2, germanCuisine);
        cuisinesRegistry.register(customer3, germanCuisine);
        cuisinesRegistry.register(customer1, frenchCuisine);
        cuisinesRegistry.register(customer2, frenchCuisine);
        cuisinesRegistry.register(customer1, italianCuisine);
        cuisinesRegistry.register(customer2, italianCuisine);
        cuisinesRegistry.register(customer1, spanishCuisine);

        List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(3);

        assertEquals(3, topCuisines.size());
        assertEquals(3, topCuisines.size());
        assertTrue(!topCuisines.contains(spanishCuisine));

        List<Cuisine> topCuisinesEmpty = cuisinesRegistry.topCuisines(0);
        assertEquals(0, topCuisinesEmpty.size());
    }
}
